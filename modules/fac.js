export default function fac(number) {
    // console.log(number)
    let ans = 1
    for (let i = 1; i <= number; i++) {
        if (ans == Infinity) {
            return Infinity
        }
        ans = ans * i
    }
    return ans
}