export default function sign(inputArray, calcArray) {
    if (isNaN(inputArray.join(""))) {
        if (inputArray[0] == "-" && inputArray[1] == "(" && inputArray[inputArray.length - 1] == ")") {
            inputArray.splice(0, 2);
            calcArray.splice(0, 2)
            inputArray.pop();
            calcArray.pop()
        }
        else {
            inputArray = ["-", "(", ...inputArray, ")"];
            calcArray = ["-", "(", ...calcArray, ")"];
        }
    }

    else {
        if (Number(inputArray.join("")) < 0) {
            inputArray.splice(0, 1);
            calcArray.splice(0, 1)
        } else if (Number(inputArray.join("")) > 0) {
            inputArray = ["-", ...inputArray];
            calcArray = ["-", ...calcArray];
        }
    }
    console.log(inputArray, calcArray)

    return { inputArray, calcArray }
}