export default function trigonometry(sign, deg) {
    // console.log(`sign ${sign} deg ${deg}`)
    let ans = eval(`Math.${sign}(${deg})`).toFixed(2)
    return ans
}