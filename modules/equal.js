import fac from "./fac.js"
import trigonometry from "./trigonometry.js"
export default function equal(inputArray, calcArray) {
    try {
        let temp = calcArray.join("")
        let ans = eval(temp)
        calcArray = [ans]
        inputArray = [ans]
        return { inputArray, calcArray }
    }
    
    catch {
        inputArray = ['Invalid Input']
        return { inputArray, calcArray }
    }
}