export default function back(inputArray, calcArray) {
    if (inputArray.length) {
        inputArray.pop()
    }
    if (calcArray.length) {
        calcArray.pop()
    }
    return { inputArray, calcArray }
}