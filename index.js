import back from "./modules/back.js";
import disable from "./modules/disable.js";
import display from "./modules/display.js";
import enable from "./modules/enable.js";
import equal from "./modules/equal.js";
import sign from "./modules/sign.js";

let memoryArr = [];

function getMemory() {
    if (localStorage.getItem('memory')) {
        memoryArr = JSON.parse(localStorage.getItem('memory'))
    }
}
getMemory()


const input = document.getElementById('input');
const mr = document.getElementById('mr')
const mc = document.getElementById('mc')
let calcArr = [];
let inputArr = [];

if (memoryArr.length === 0) {
    disable(mr)
    disable(mc)
}

document.querySelectorAll('.btn,.hover-btn,#deg,#fe').forEach((i) => {
    i.addEventListener('click', (e) => {
        e.preventDefault()
        if (e.target.className.includes('simple')) {

            if (e.target.id == 'pie') {
                pushValue('π', Math.PI)
            }

            else if (e.target.id == 'e') {
                pushValue('e', Math.E)
            }

            else if (e.target.id == 'pow') {
                pushValue("^", "**")
            }

            else {
                pushValue(e.target.innerHTML, e.target.value)
            }
        }

        else {
            let t, obj

            switch (e.target.id) {

                case 'fac':
                    pushValue("fac(", "fac(")
                    break;

                case "deg":
                    obj = equal(inputArr, calcArr);
                    inputArr = obj.inputArray
                    calcArr = obj.calcArray
                    t = calcArr.join("")
                    t = (t * (180 / Math.PI)).toFixed(2)
                    calcArr = [t]
                    inputArr = [t]
                    break;

                case "fe":
                    obj = equal(inputArr, calcArr);
                    inputArr = obj.inputArray
                    calcArr = obj.calcArray
                    t = parseFloat(calcArr.join(""))
                    t = t.toExponential()
                    calcArr = [t]
                    inputArr = [t]
                    break;

                case "ten-pow":
                    pushValue('10^', '10**')
                    break;

                case 'log':
                    pushValue('log(', "Math.log10(")
                    break;

                case 'ln':
                    pushValue('ln(', "Math.log(")
                    break;

                case 'sign':
                    obj = sign(inputArr, calcArr);
                    inputArr = obj.inputArray
                    calcArr = obj.calcArray
                    break;

                case 'root':
                    pushValue('root(', "Math.sqrt(")
                    break;

                case 'sin':
                    pushValue('sin(', "trigonometry('sin',")
                    break;

                case 'cos':
                    pushValue('cos(', "trigonometry('cos',")
                    break;

                case 'tan':
                    pushValue('tan(', "trigonometry('tan',")
                    break;

                case 'sinh':
                    pushValue('sinh(', "trigonometry('sinh',")
                    break;

                case 'cosh':
                    pushValue('cosh(', "trigonometry('cosh',")
                    break;

                case 'tanh':
                    pushValue("tanh(", "trigonometry('tanh',")
                    break;

                case 'square':
                    pushValue('^2', "**2")
                    break;

                case 'divBy':
                    obj = equal(inputArr, calcArr);
                    inputArr = obj.inputArray
                    calcArr = obj.calcArray
                    calcArr = [eval(1 / calcArr.join(""))]
                    inputArr = [calcArr]
                    break;

                case 'absolute':
                    pushValue('abs(', "Math.abs(")
                    break;

                case 'exp':
                    pushValue('exp(', 'Math.exp(')
                    break;

                case 'ceil':
                    pushValue('ceil(', 'Math.ceil(')
                    break;

                case 'floor':
                    pushValue('floor(', 'Math.floor(')
                    break;

                case 'equal':
                    obj = equal(inputArr, calcArr);
                    inputArr = obj.inputArray
                    calcArr = obj.calcArray
                    display(inputArr, calcArr, input)
                    calcArr = []
                    inputArr = []
                    return

                case 'clear':
                    calcArr = []
                    inputArr = []
                    break;

                case 'back':
                    obj = back(inputArr, calcArr);
                    inputArr = obj.inputArray
                    calcArr = obj.calcArray
                    break;

                default:
                    break;
            }
        }
        display(inputArr, calcArr, input)
    })
})

document.querySelectorAll('.memory').forEach((i) => {
    i.addEventListener('click', (e) => {
        e.preventDefault();

        if (e.target.className.includes('disable')) {
            return;
        }

        let value, obj
        switch (e.target.id) {

            case 'm+':
                obj = equal(inputArr, calcArr);
                inputArr = obj.inputArray
                calcArr = obj.calcArray
                if (!calcArr.length) {
                    value = 0
                }

                else {
                    value = calcArr.join("")
                }

                memoryArr.push(value * 1)
                localStorage.setItem('memory', JSON.stringify(memoryArr))
                enable(mr)
                enable(mc)
                display(inputArr, calcArr, input)
                break;

            case 'm-':
                obj = equal(inputArr, calcArr);
                inputArr = obj.inputArray
                calcArr = obj.calcArray
                if (!calcArr.length) {
                    value = 0
                }

                else {
                    value = calcArr.join("")
                }

                memoryArr.push(value * -1)
                localStorage.setItem('memory', JSON.stringify(memoryArr))
                enable(mr)
                enable(mc)
                display(inputArr, calcArr, input)
                break;

            case 'mc':
                memoryArr = []
                localStorage.clear()
                disable(mr)
                disable(mc)
                break;

            case 'mr':
                getMemory()
                try {
                    let temp = memoryArr.join("+")
                    temp = eval(temp)
                    memoryArr = [temp]
                    inputArr = [temp]
                    calcArr = [temp]
                    localStorage.setItem('memory', JSON.stringify(memoryArr))
                    display(inputArr, calcArr, input)
                }

                catch {
                    input.value = 'Invalid Input'
                }
                break;

            default:
                break;
        }
    })
})

function pushValue(inputValue, calcValue) {
    inputArr.push(inputValue)
    calcArr.push(calcValue)
}